#ifndef MAINWINDOW_H
#define MAINWINDOW_H

/**
 * \file
 * \brief Definicja klasy MainWindow
 *
 * Plik zawiera definicje klasy MainWindow
 * odpowiedzialnej za dzialanie glownego okna programu
 */

#include "ui_mainwindow.h"
#include <QMainWindow>
#include <QtSerialPort/QSerialPort>
#include <QtSerialPort/QSerialPortInfo>
#include <QMessageBox>
#include <QDialog>
#include <QDebug>
#include <QString>
#include <QTime>
#include <string>

namespace Ui {class MainWindow;}


/*!
 * \brief Modeluje okno glowne aplikacji
 *
 * Modeluje okno glowne aplikacji
 */
class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    /*!
     * \brief Inicjalizuje poczatkowy wyglad okna glownego
     *
     * Inicjalizuje poczatkowy wyglad okna glownego
     */
    explicit MainWindow(QWidget *parent = 0);
    /*!
     * Destruktor
     */
    ~MainWindow();

private slots:
    /*!
     * \brief Odczytywanie danych z czujnikow
     *
     * Odczytywanie danych z czujnikow
     */
    void readSerial();


    /*!
     * \brief Laczenie z czujnikami
     *
     * Laczenie z czujnikami
     */
    void on_connect_clicked();
    /*!
     * \brief Rzolaczanie z czujnikami
     *
     * Rozlaczanie z czujnikami
     */
    void on_disconnect_clicked();
    /*!
     * \brief Ustawianie limitow odleglosci
     *
     * Ustawianie limitow odleglosci
     */
    void on_change_limits_clicked();
    /*!
     * \brief Konczenie pracy programu
     *
     * Konczenie pracy programu
     */
    void on_exit_clicked();


    /*!
     * \brief Podjecie akcji polaczenia z menu opcji
     *
     * Podjecie akcji polaczenia z menu opcji
     */
    void on_actionConnect_triggered();
    /*!
     * \brief Podjecie akcji rozlaczenia z menu opcji
     *
     * Podjecie akcji rozlaczenia z menu opcji
     */
    void on_actionDisconnect_triggered();
    /*!
     * \brief Podjecie akcji zmiany limitow z menu opcji
     *
     * Podjecie akcji zmiany limitow z menu opcji
     */
    void on_actionChange_limits_triggered();
    /*!
     * \brief Podjecie akcji zakonczenia programu z menu opcji
     *
     * Podjecie akcji zakonczenia programu z menu opcji
     */
    void on_actionExit_triggered();


    /*!
     * \brief Aktualizacja stanu czterech wyswietlaczy LCD
     *
     * Aktualizacja stanu czterech wyswietlaczy LCD
     * \param[in] sensor_reading - odczyt z prawego czujnika
     * \param[in] sensor_reading2 - odczyt z przedniego prawego czujnika
     * \param[in] sensor_reading3 - odczyt z przedniego lewego czujnika
     * \param[in] sensor_reading4 - odczyt z lewego czujnika
     */
    void updateLCD(const QString sensor_reading,const QString sensor_reading2,const QString sensor_reading3,const QString sensor_reading4);
    /*!
     * \brief Aktualizaja stanu czterech wykresow odleglosci od czasu
     *
     * Aktualizaja stanu czterech wykresow odleglosci od czasu
     * \param[in] actualTime - czas odczytu
     * \param[in] lcd1_value - wartosc odleglosci z pierwszego wyswietlacza LCD
     * \param[in] lcd2_value - wartosc odleglosci z drugiego wyswietlacza LCD
     * \param[in] lcd3_value - wartosc odleglosci z trzeciego wyswietlacza LCD
     * \param[in] lcd4_value - wartosc odleglosci z czwartego wyswietlacza LCD
     */
    void updatePlots(double actualTime,double lcd1_value,double lcd2_value,double lcd3_value,double lcd4_value);
    /*!
     * \brief Aktualizacja widoku 2D
     *
     * Aktualizacja widoku 2D
     * \param[in] lcd1_value - wartosc odleglosci z pierwszego wyswietlacza LCD
     * \param[in] lcd2_value - wartosc odleglosci z drugiego wyswietlacza LCD
     * \param[in] lcd3_value - wartosc odleglosci z trzeciego wyswietlacza LCD
     * \param[in] lcd4_value - wartosc odleglosci z czwartego wyswietlacza LCD
     */
    void updateView(double lcd1_value,double lcd2_value,double lcd3_value,double lcd4_value );



    /*!
     * \brief Wstepne ustawienia wykresow
     *
     * Wstepne ustawienia wykresow
     */
    void setupPlots();
    /*!
     * \brief Przeladowanie wykresow do stanu poczatkowego
     *
     * Przeladowanie wykresow do stanu poczatkowego
     */
    void reloadPlots();

private:
    Ui::MainWindow *ui;

    QSerialPort *arduino;
    static const quint16 arduino_uno_vendor_id =9025;
    static const quint16 arduino_uno_product_id=67;
    QByteArray serialData;
    QString serialBuffer,text;

    QMessageBox question;
    QPushButton *yesButton =question.addButton(QMessageBox::Yes);
    QPushButton *noButton =question.addButton(QMessageBox::No);

    QCPScatterStyle myScatter;

    /*!
     * \brief Pomocniczy licznik
     *
     * Pomocniczy licznik
     */
    int counter=0;
    /*!
     * \brief Wartosci mozliwych limitow
     *
     * Wartosci mozliwych limitow
     */
    int limitTab[20]={200,100,50,25,10,200,120,80,40,15,200,150,100,50,25,200,150,100,60,30};
    /*!
     * \brief Aktualnie wybrane limity
     *
     * Aktualnie wybrane limity
     */
    int limit[5];
    /*!
     * \brief Licznik czasu wykresow
     *
     * Licznik czasu wykresow
     */
    long int tmp=0;
};
#endif // MAINWINDOW_H
