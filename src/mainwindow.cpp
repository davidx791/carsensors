/**
 * \file
 * \brief Definicja metod klasy MainWindow
 *
 * Plik zawiera definicje metod klasy MainWindow
 * odpowiedzialnej za dzialanie glownego okna programu
 */

#include "inc/mainwindow.h"

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this); //ustawienia z designera

    /***WSTEPNE USTAWIENIA WIZUALNE***/
    ui->lcd_right->display("------");
    ui->lcd_front_right->display("------");
    ui->lcd_front_left->display("------");
    ui->lcd_left->display("------");
    ui->czujnik1_stan->setStyleSheet("background-color:white;");
    ui->czujnik2_stan->setStyleSheet("background-color:white;");
    ui->czujnik3_stan->setStyleSheet("background-color:white;");
    ui->czujnik4_stan->setStyleSheet("background-color:white;");
    ui->czujnik1->setStyleSheet("background-color:white;");
    ui->czujnik2->setStyleSheet("background-color:white;");
    ui->czujnik3->setStyleSheet("background-color:white;");
    ui->czujnik4->setStyleSheet("background-color:white;");
    ui->actionConnect->setIcon(QPixmap(QString::fromUtf8(":/img/wifi.png")));
    ui->actionDisconnect->setIcon(QPixmap(QString::fromUtf8(":/img/nowifi.png")));
    ui->actionChange_limits->setIcon(QPixmap(QString::fromUtf8(":/img/limit.png")));
    ui->actionExit->setIcon(QPixmap(QString::fromUtf8(":/img/exit.png")));

    /***USTAWIENIA PRZYISKOW***/
    ui->disconnect->setDisabled(1);
    ui->actionDisconnect->setDisabled(1);
    ui->connect->setStyleSheet("background-color:white");
    ui->disconnect->setStyleSheet("background-color:white");
    ui->change_limits->setStyleSheet("background-color:white");
    ui->exit->setStyleSheet("background-color:white");

    /***USTAWIENIA LIMITOW I LEGENDY***/
    for(int i=0;i<5;i++)
        limit[i]=limitTab[i];
    ui->legend->setPixmap(QPixmap(QString::fromUtf8(":/img/legend1.png")));

    /***USTAWIENIA DODATKOWEGO OKIENKA***/
    question.setWindowIcon(QPixmap(QString::fromUtf8(":/img/pytajnik.png")));
    question.setWindowTitle("Question");
    question.setText("Are you sure?");

    //***USTAWIENIA WYKRESOW***/
    setupPlots();
    reloadPlots();
}

/***TWORZENIE POLACZENIA***/
void MainWindow::on_connect_clicked()
{
    text.insert(0,"Próba podłączenia\n");
    ui->textBrowser->setText(text);
    arduino=new QSerialPort(this);
    serialBuffer="";
    bool arduino_is_available=false;
    QString arduino_uno_port_name;
    foreach(const QSerialPortInfo &serialPortInfo, QSerialPortInfo::availablePorts())
    {
       //qDebug()<<serialPortInfo.productIdentifier()<<" "<<serialPortInfo.vendorIdentifier();
       if(serialPortInfo.hasProductIdentifier() && serialPortInfo.hasVendorIdentifier())
        {
           //dostępność płytki arduino
            if( (serialPortInfo.productIdentifier() == arduino_uno_product_id)
                    && (serialPortInfo.vendorIdentifier() == arduino_uno_vendor_id) )
            {
                arduino_is_available  =true;
                arduino_uno_port_name=serialPortInfo.portName();
            }
        }
    }
    if(arduino_is_available){ //gdy Arduino dostępne ustawiamy parametry
        arduino->setPortName(arduino_uno_port_name);
        arduino->open(QSerialPort::ReadOnly);
        arduino->setBaudRate(QSerialPort::Baud9600);
        arduino->setDataBits(QSerialPort::Data8);
        arduino->setFlowControl(QSerialPort::NoFlowControl);
        arduino->setParity(QSerialPort::NoParity);
        arduino->setStopBits(QSerialPort::OneStop);
        reloadPlots();
        QObject::connect(arduino,SIGNAL(readyRead()),this,SLOT(readSerial())); //łączymy

        text.insert(0,"Podłączono\n");
        ui->textBrowser->setText(text);

        //zmiana blokad przyciskow i odpowiadajacych opcji
        ui->connect->setDisabled(1);
        ui->disconnect->setEnabled(1);
        ui->actionConnect->setDisabled(1);
        ui->actionDisconnect->setEnabled(1);
    }
    else{ // gdy nie mamy połączenia z Arduino
        text.insert(0,"Nie można połączyć z Arduino\n");
        ui->textBrowser->setText(text);
        ui->connect->setEnabled(1);
        ui->disconnect->setDisabled(2);
        ui->actionConnect->setEnabled(1);
        ui->actionDisconnect->setDisabled(1);
    }
}

/***ZAKONCZANIE POLACZENIA***/
void MainWindow::on_disconnect_clicked()
{
    text.insert(0,"Czy na pewno rozłączyć?\n");
    ui->textBrowser->setText(text);
    question.setDefaultButton(noButton);
    question.exec();

    if(question.clickedButton() == yesButton) //jesli potwierdzono rozlaczenie
    {
        text.insert(0,"Rozłączanie...\n");
        ui->textBrowser->setText(text);
        //zmiana blokad przyciskow i odpowiadajacych opcji
        ui->connect->setEnabled(1);
        ui->disconnect->setDisabled(1);
        ui->actionConnect->setEnabled(1);
        ui->actionDisconnect->setDisabled(1);
        if(arduino->isOpen())
            arduino->close();

        /***zmiana ustawien wizualnych na podstawowe***/
        ui->lcd_right->display("------");
        ui->lcd_front_right->display("------");
        ui->lcd_front_left->display("------");
        ui->lcd_left->display("------");
        ui->topView2D->setPixmap(QPixmap(QString::fromUtf8(":/img/t0.png")));
        ui->bottomView2D->setPixmap(QPixmap(QString::fromUtf8(":/img/b0.png")));
        ui->czujnik1_stan->setStyleSheet("background-color:white;");
        ui->czujnik2_stan->setStyleSheet("background-color:white;");
        ui->czujnik3_stan->setStyleSheet("background-color:white;");
        ui->czujnik4_stan->setStyleSheet("background-color:white;");
        ui->plot1->setBackground(QBrush(Qt::white));
        ui->plot2->setBackground(QBrush(Qt::white));
        ui->plot3->setBackground(QBrush(Qt::white));
        ui->plot4->setBackground(QBrush(Qt::white));
        ui->warning->setPixmap(QPixmap(QString::fromUtf8(":/img/warning.png")));
        text.insert(0,"Rozłączono\n");
        ui->textBrowser->setText(text);
    }
}

/***KONCZENIE PRACY PROGRAMU***/
void MainWindow::on_exit_clicked()
{
    text.insert(0,"Czy na pewno zakończyć pracę programu?\n");
    ui->textBrowser->setText(text);
    question.setDefaultButton(noButton);
    question.exec();
    if(question.clickedButton() == yesButton)
            exit(0);
}

/***ZMIANA LIMITOW***/
void MainWindow::on_change_limits_clicked()
{
    counter=counter+5;
    if(counter==20) //reset licznika
        counter=0;
    for(int i=0;i<5;i++) //ustawienia limitow
        limit[i]=limitTab[i+counter];

    /***INFORMACJE I USTAWIENIA LEGENDY***/
    if(counter==0)
    {
        text.insert(0,"Ustawiono limity: 200-100-50-25-10 cm\n");
        ui->textBrowser->setText(text);
        ui->legend->setPixmap(QPixmap(QString::fromUtf8(":/img/legend1.png")));
    }
    if(counter==5)
    {
        text.insert(0,"Ustawiono limity: 200-120-80-40-15 cm\n");
        ui->textBrowser->setText(text);
        ui->legend->setPixmap(QPixmap(QString::fromUtf8(":/img/legend2.png")));
    }
    if(counter==10)
     {
        text.insert(0,"Ustawiono limity: 200-150-100-50-25 cm\n");
        ui->textBrowser->setText(text);
        ui->legend->setPixmap(QPixmap(QString::fromUtf8(":/img/legend3.png")));
    }
    if(counter==15)
     {
        text.insert(0,"Ustawiono limity: 200-150-100-60-30 cm\n");
        ui->textBrowser->setText(text);
        ui->legend->setPixmap(QPixmap(QString::fromUtf8(":/img/legend4.png")));
     }
}

/***ODCZYTYWANIE DANYCH***/
void MainWindow::readSerial()
{
    QStringList bufferSplit=serialBuffer.split(",");
    if(bufferSplit.length() < 6)
    {
       serialData=arduino->readAll();
       serialBuffer+=QString::fromStdString(serialData.toStdString());
       if(bufferSplit.length()==2 && bufferSplit[0]!="!")
       {
          serialBuffer="";
          bufferSplit.clear();
       }
    }
    else
    {
        if(bufferSplit[0]=="!")
        {
            static QTime time(QTime::currentTime());
            if (tmp==0)
            {
                time.restart();
                ui->plot1->graph()->data()->clear();
                ui->plot2->graph()->data()->clear();
                ui->plot3->graph()->data()->clear();
                ui->plot4->graph()->data()->clear();
            }
            double actualTime = time.elapsed()/1000.0;
            bool ok = false;
            /***odczytanie wartosci i konwersja stringa na double***/
            double lcd1_value = bufferSplit[1].toDouble(&ok);
            double lcd2_value = bufferSplit[2].toDouble(&ok);
            double lcd3_value = bufferSplit[3].toDouble(&ok);
            double lcd4_value = bufferSplit[4].toDouble(&ok);

            /*aktualizacja całego widoku aplikacji*/
            updatePlots(actualTime,lcd1_value,lcd2_value,lcd3_value,lcd4_value);
            updateLCD(bufferSplit[1],bufferSplit[2],bufferSplit[3],bufferSplit[4]);
            updateView(lcd1_value,lcd2_value,lcd3_value,lcd4_value);
            tmp++;
            serialBuffer=""; //zerujemy bufor
        }
        else
            serialBuffer="";
    }
}

/***POLACZENIE Z OPCJAMI NA PASKU MENU ***/
void MainWindow::on_actionConnect_triggered()       {   on_connect_clicked();        }
void MainWindow::on_actionDisconnect_triggered()    {   on_disconnect_clicked();     }
void MainWindow::on_actionExit_triggered()          {   on_exit_clicked();           }
void MainWindow::on_actionChange_limits_triggered() {   on_change_limits_clicked();  }

/***AKTUALIZACJA LCD***/
void MainWindow::updateLCD(const QString sensor_reading,const QString sensor_reading2,const QString sensor_reading3,const QString sensor_reading4)
{
    ui->lcd_right->display(sensor_reading);
    ui->lcd_front_right->display(sensor_reading2);
    ui->lcd_front_left->display(sensor_reading3);
    ui->lcd_left->display(sensor_reading4);
    if(sensor_reading == "------")
        ui->czujnik1_stan->setStyleSheet("background-color:white;");
    if(sensor_reading2 == "------")
        ui->czujnik2_stan->setStyleSheet("background-color:white;");
    if(sensor_reading3 == "------")
        ui->czujnik3_stan->setStyleSheet("background-color:white;");
    if(sensor_reading4 == "------")
        ui->czujnik4_stan->setStyleSheet("background-color:white;");
}

/***AKTUALIZACJA WYKRESOW***/
void MainWindow::updatePlots(double actualTime,double lcd1_value,double lcd2_value,double lcd3_value,double lcd4_value)
{
   ui->plot1->setBackground(QBrush(Qt::white));
   ui->plot2->setBackground(QBrush(Qt::white));
   ui->plot3->setBackground(QBrush(Qt::white));
   ui->plot4->setBackground(QBrush(Qt::white));

   /*KOLOROWANIE WYKRESOW*/
   if(lcd1_value<limit[2])
    ui->plot1->setBackground(QBrush(Qt::yellow));
   if(lcd1_value<limit[3])
    ui->plot1->setBackground(QBrush(QColor("#FFA500")));
   if(lcd1_value<limit[4])
    ui->plot1->setBackground(QBrush(Qt::red));

   if(lcd2_value<limit[2])
    ui->plot2->setBackground(QBrush(Qt::yellow));
   if(lcd2_value<limit[3])
    ui->plot2->setBackground(QBrush(QColor("#FFA500")));
   if(lcd2_value<limit[4])
    ui->plot2->setBackground(QBrush(Qt::red));

   if(lcd3_value<limit[2])
    ui->plot3->setBackground(QBrush(Qt::yellow));
   if(lcd3_value<limit[3])
    ui->plot3->setBackground(QBrush(QColor("#FFA500")));
   if(lcd3_value<limit[4])
    ui->plot3->setBackground(QBrush(Qt::red));

   if(lcd4_value<limit[2])
    ui->plot4->setBackground(QBrush(Qt::yellow));
   if(lcd4_value<limit[3])
    ui->plot4->setBackground(QBrush(QColor("#FFA500")));
   if(lcd4_value<limit[4])
    ui->plot4->setBackground(QBrush(Qt::red));

   /*DODAWANIE NOWYCH DANYCH*/
    ui->plot1->graph()->addData(actualTime,lcd1_value);
    ui->plot2->graph()->addData(actualTime,lcd2_value);
    ui->plot3->graph()->addData(actualTime,lcd3_value);
    ui->plot4->graph()->addData(actualTime,lcd4_value);
    if(actualTime>10)
    {
        ui->plot1->xAxis->setRange(actualTime, 10, Qt::AlignRight);
        ui->plot2->xAxis->setRange(actualTime, 10, Qt::AlignRight);
        ui->plot3->xAxis->setRange(actualTime, 10, Qt::AlignRight);
        ui->plot4->xAxis->setRange(actualTime, 10, Qt::AlignRight);
    }
    ui->plot1->replot();
    ui->plot2->replot();
    ui->plot3->replot();
    ui->plot4->replot();
}


/***USTAWIENIA WYKRESOW***/
void MainWindow::setupPlots()
{
    myScatter.setShape(QCPScatterStyle::ssCross);
    myScatter.setPen(QPen(Qt::blue));
    myScatter.setSize(7);
    ui->plot1->addGraph();
    ui->plot2->addGraph();
    ui->plot3->addGraph();
    ui->plot4->addGraph();
    ui->plot1->xAxis->setLabel("Czas pomiaru [s]");
    ui->plot1->yAxis->setLabel("Odległość [cm]");
    ui->plot2->xAxis->setLabel("Czas pomiaru [s]");
    ui->plot2->yAxis->setLabel("Odległość [cm]");
    ui->plot3->xAxis->setLabel("Czas pomiaru [s]");
    ui->plot3->yAxis->setLabel("Odległość [cm]");
    ui->plot4->xAxis->setLabel("Czas pomiaru [s]");
    ui->plot4->yAxis->setLabel("Odległość [cm]");
    ui->plot1->graph()->setScatterStyle(myScatter);
    ui->plot2->graph()->setScatterStyle(myScatter);
    ui->plot3->graph()->setScatterStyle(myScatter);
    ui->plot4->graph()->setScatterStyle(myScatter);
}

/***PRZELADOWANIE WYKRESOW***/
 void MainWindow::reloadPlots()
{
  ui->plot1->xAxis->setRange(0,10,Qt::AlignLeft);
  ui->plot1->yAxis->setRange(-25, 250, Qt::AlignLeft);
  ui->plot2->xAxis->setRange(0,10,Qt::AlignLeft);
  ui->plot2->yAxis->setRange(-25, 250, Qt::AlignLeft);
  ui->plot3->xAxis->setRange(0,10,Qt::AlignLeft);
  ui->plot3->yAxis->setRange(-25, 250, Qt::AlignLeft);
  ui->plot4->xAxis->setRange(0,10,Qt::AlignLeft);
  ui->plot4->yAxis->setRange(-25, 250, Qt::AlignLeft);
  tmp=0;
}

/***AKTUALIZACJA WIDOKU***/
void MainWindow::updateView(double lcd1_value,double lcd2_value,double lcd3_value,double lcd4_value )
{


   /***KOLOROWANIE STATUSOW CZUJNIKOW I WARNINGOW***/
   ui->warning->setPixmap(QPixmap(QString::fromUtf8(":/img/warning.png")));
    if(lcd1_value>=limit[2])
    ui->czujnik1_stan->setStyleSheet("background-color:lime;");
   if(lcd1_value<limit[2])
    ui->czujnik1_stan->setStyleSheet("background-color:yellow;");
   if(lcd1_value<limit[3])
   {
       ui->warning->setPixmap(QPixmap(QString::fromUtf8(":/img/warning2.png")));
       ui->czujnik1_stan->setStyleSheet("background-color:orange;");
   }
   if(lcd1_value<limit[4])
   {
       ui->warning->setPixmap(QPixmap(QString::fromUtf8(":/img/warning2.png")));
       ui->czujnik1_stan->setStyleSheet("background-color:red;");
   }
   if(lcd2_value>=limit[2])
    ui->czujnik2_stan->setStyleSheet("background-color:lime;");
   if(lcd2_value<limit[2])
    ui->czujnik2_stan->setStyleSheet("background-color:yellow;");
   if(lcd2_value<limit[3])
   {
       ui->warning->setPixmap(QPixmap(QString::fromUtf8(":/img/warning2.png")));
       ui->czujnik2_stan->setStyleSheet("background-color:orange;");
   }
   if(lcd2_value<limit[4])
   {
       ui->warning->setPixmap(QPixmap(QString::fromUtf8(":/img/warning2.png")));
       ui->czujnik2_stan->setStyleSheet("background-color:red;");
   }
   if(lcd3_value>=limit[2])
    ui->czujnik3_stan->setStyleSheet("background-color:lime;");
   if(lcd3_value<limit[2])
    ui->czujnik3_stan->setStyleSheet("background-color:yellow;");
   if(lcd3_value<limit[3])
   {
       ui->warning->setPixmap(QPixmap(QString::fromUtf8(":/img/warning2.png")));
        ui->czujnik3_stan->setStyleSheet("background-color:orange;");
   }
   if(lcd3_value<limit[4])
   {
       ui->warning->setPixmap(QPixmap(QString::fromUtf8(":/img/warning2.png")));
       ui->czujnik3_stan->setStyleSheet("background-color:red;");
   }
   if(lcd4_value>=limit[2])
    ui->czujnik4_stan->setStyleSheet("background-color:lime;");
   if(lcd4_value<limit[2])
    ui->czujnik4_stan->setStyleSheet("background-color:yellow;");
   if(lcd4_value<limit[3])
   {
       ui->warning->setPixmap(QPixmap(QString::fromUtf8(":/img/warning2.png")));
       ui->czujnik4_stan->setStyleSheet("background-color:orange;");
   }
   if(lcd4_value<limit[4])
   {
       ui->warning->setPixmap(QPixmap(QString::fromUtf8(":/img/warning2.png")));
       ui->czujnik4_stan->setStyleSheet("background-color:red;");
   }

   /***AKTUALIZACJA WIDOKU 2D***/
   /***USTAWIENIE WLASCIWEGO OBRAZKA DOLNEGO***/
   if(lcd2_value>=limit[0])
    {
      if(lcd1_value>=limit[0])
        ui->topView2D->setPixmap(QPixmap(QString::fromUtf8(":/img/t0.png")));
      if(lcd1_value<limit[0])
        ui->topView2D->setPixmap(QPixmap(QString::fromUtf8(":/img/t1.png")));
      if(lcd1_value<limit[1])
        ui->topView2D->setPixmap(QPixmap(QString::fromUtf8(":/img/t2.png")));
      if(lcd1_value<limit[2])
        ui->topView2D->setPixmap(QPixmap(QString::fromUtf8(":/img/t3.png")));
      if(lcd1_value<limit[3])
        ui->topView2D->setPixmap(QPixmap(QString::fromUtf8(":/img/t4.png")));
      if(lcd1_value<limit[4])
        ui->topView2D->setPixmap(QPixmap(QString::fromUtf8(":/img/t5.png")));
   }
   if(lcd2_value<limit[0])
   {
      if(lcd1_value>=limit[0])
        ui->topView2D->setPixmap(QPixmap(QString::fromUtf8(":/img/t6.png")));
      if(lcd1_value<limit[0])
        ui->topView2D->setPixmap(QPixmap(QString::fromUtf8(":/img/t7.png")));
      if(lcd1_value<limit[1])
        ui->topView2D->setPixmap(QPixmap(QString::fromUtf8(":/img/t8.png")));
      if(lcd1_value<limit[2])
        ui->topView2D->setPixmap(QPixmap(QString::fromUtf8(":/img/t9.png")));
      if(lcd1_value<limit[3])
        ui->topView2D->setPixmap(QPixmap(QString::fromUtf8(":/img/t10.png")));
      if(lcd1_value<limit[4])
        ui->topView2D->setPixmap(QPixmap(QString::fromUtf8(":/img/t11.png")));
    }
    if(lcd2_value<limit[1])
    {
      if(lcd1_value>=limit[0])
        ui->topView2D->setPixmap(QPixmap(QString::fromUtf8(":/img/t12.png")));
      if(lcd1_value<limit[0])
        ui->topView2D->setPixmap(QPixmap(QString::fromUtf8(":/img/t13.png")));
      if(lcd1_value<limit[1])
        ui->topView2D->setPixmap(QPixmap(QString::fromUtf8(":/img/t14.png")));
      if(lcd1_value<limit[2])
        ui->topView2D->setPixmap(QPixmap(QString::fromUtf8(":/img/t15.png")));
      if(lcd1_value<limit[3])
        ui->topView2D->setPixmap(QPixmap(QString::fromUtf8(":/img/t16.png")));
      if(lcd1_value<limit[4])
        ui->topView2D->setPixmap(QPixmap(QString::fromUtf8(":/img/t17.png")));
    }
    if(lcd2_value<limit[2])
    {
      if(lcd1_value>=limit[0])
        ui->topView2D->setPixmap(QPixmap(QString::fromUtf8(":/img/t18.png")));
      if(lcd1_value<limit[0])
        ui->topView2D->setPixmap(QPixmap(QString::fromUtf8(":/img/t19.png")));
      if(lcd1_value<limit[1])
        ui->topView2D->setPixmap(QPixmap(QString::fromUtf8(":/img/t20.png")));
      if(lcd1_value<limit[2])
        ui->topView2D->setPixmap(QPixmap(QString::fromUtf8(":/img/t21.png")));
      if(lcd1_value<limit[3])
        ui->topView2D->setPixmap(QPixmap(QString::fromUtf8(":/img/t22.png")));
      if(lcd1_value<limit[4])
        ui->topView2D->setPixmap(QPixmap(QString::fromUtf8(":/img/t23.png")));
    }
    if(lcd2_value<limit[3])
    {
      if(lcd1_value>=limit[0])
        ui->topView2D->setPixmap(QPixmap(QString::fromUtf8(":/img/t24.png")));
      if(lcd1_value<limit[0])
        ui->topView2D->setPixmap(QPixmap(QString::fromUtf8(":/img/t25.png")));
      if(lcd1_value<limit[1])
        ui->topView2D->setPixmap(QPixmap(QString::fromUtf8(":/img/t26.png")));
      if(lcd1_value<limit[2])
        ui->topView2D->setPixmap(QPixmap(QString::fromUtf8(":/img/t27.png")));
      if(lcd1_value<limit[3])
        ui->topView2D->setPixmap(QPixmap(QString::fromUtf8(":/img/t28.png")));
      if(lcd1_value<limit[4])
        ui->topView2D->setPixmap(QPixmap(QString::fromUtf8(":/img/t29.png")));
    }
    if(lcd2_value<limit[4])
    {
      if(lcd1_value>=limit[0])
        ui->topView2D->setPixmap(QPixmap(QString::fromUtf8(":/img/t30.png")));
      if(lcd1_value<limit[0])
        ui->topView2D->setPixmap(QPixmap(QString::fromUtf8(":/img/t31.png")));
      if(lcd1_value<limit[1])
        ui->topView2D->setPixmap(QPixmap(QString::fromUtf8(":/img/t32.png")));
      if(lcd1_value<limit[2])
        ui->topView2D->setPixmap(QPixmap(QString::fromUtf8(":/img/t33.png")));
      if(lcd1_value<limit[3])
        ui->topView2D->setPixmap(QPixmap(QString::fromUtf8(":/img/t34.png")));
      if(lcd1_value<limit[4])
        ui->topView2D->setPixmap(QPixmap(QString::fromUtf8(":/img/t35.png")));
    }

    /***USTAWIENIE WLASCIWEGO OBRAZKA DOLNEGO***/
   if(lcd4_value>=limit[0])
    {
      if(lcd3_value>=limit[0])
        ui->bottomView2D->setPixmap(QPixmap(QString::fromUtf8(":/img/b0.png")));
      if(lcd3_value<limit[0])
        ui->bottomView2D->setPixmap(QPixmap(QString::fromUtf8(":/img/b1.png")));
      if(lcd3_value<limit[1])
        ui->bottomView2D->setPixmap(QPixmap(QString::fromUtf8(":/img/b2.png")));
      if(lcd3_value<limit[2])
        ui->bottomView2D->setPixmap(QPixmap(QString::fromUtf8(":/img/b3.png")));
      if(lcd3_value<limit[3])
        ui->bottomView2D->setPixmap(QPixmap(QString::fromUtf8(":/img/b4.png")));
      if(lcd3_value<limit[4])
        ui->bottomView2D->setPixmap(QPixmap(QString::fromUtf8(":/img/b5.png")));
   }
   if(lcd4_value<limit[0])
   {
      if(lcd3_value>=limit[0])
        ui->bottomView2D->setPixmap(QPixmap(QString::fromUtf8(":/img/b6.png")));
      if(lcd3_value<limit[0])
        ui->bottomView2D->setPixmap(QPixmap(QString::fromUtf8(":/img/b7.png")));
      if(lcd3_value<limit[1])
        ui->bottomView2D->setPixmap(QPixmap(QString::fromUtf8(":/img/b8.png")));
      if(lcd3_value<limit[2])
        ui->bottomView2D->setPixmap(QPixmap(QString::fromUtf8(":/img/b9.png")));
      if(lcd3_value<limit[3])
        ui->bottomView2D->setPixmap(QPixmap(QString::fromUtf8(":/img/b10.png")));
      if(lcd3_value<limit[4])
        ui->bottomView2D->setPixmap(QPixmap(QString::fromUtf8(":/img/b11.png")));
    }
    if(lcd4_value<limit[1])
    {
      if(lcd3_value>=limit[0])
        ui->bottomView2D->setPixmap(QPixmap(QString::fromUtf8(":/img/b12.png")));
      if(lcd3_value<limit[0])
        ui->bottomView2D->setPixmap(QPixmap(QString::fromUtf8(":/img/b13.png")));
      if(lcd3_value<limit[1])
        ui->bottomView2D->setPixmap(QPixmap(QString::fromUtf8(":/img/b14.png")));
      if(lcd3_value<limit[2])
        ui->bottomView2D->setPixmap(QPixmap(QString::fromUtf8(":/img/b15.png")));
      if(lcd3_value<limit[3])
        ui->bottomView2D->setPixmap(QPixmap(QString::fromUtf8(":/img/b16.png")));
      if(lcd3_value<limit[4])
        ui->bottomView2D->setPixmap(QPixmap(QString::fromUtf8(":/img/b17.png")));
    }
    if(lcd4_value<limit[2])
    {
      if(lcd3_value>=limit[0])
        ui->bottomView2D->setPixmap(QPixmap(QString::fromUtf8(":/img/b18.png")));
      if(lcd3_value<limit[0])
        ui->bottomView2D->setPixmap(QPixmap(QString::fromUtf8(":/img/b19.png")));
      if(lcd3_value<limit[1])
        ui->bottomView2D->setPixmap(QPixmap(QString::fromUtf8(":/img/b20.png")));
      if(lcd3_value<limit[2])
        ui->bottomView2D->setPixmap(QPixmap(QString::fromUtf8(":/img/b21.png")));
      if(lcd3_value<limit[3])
        ui->bottomView2D->setPixmap(QPixmap(QString::fromUtf8(":/img/b22.png")));
      if(lcd3_value<limit[4])
        ui->bottomView2D->setPixmap(QPixmap(QString::fromUtf8(":/img/b23.png")));
    }
    if(lcd4_value<limit[3])
    {
      if(lcd3_value>=limit[0])
        ui->bottomView2D->setPixmap(QPixmap(QString::fromUtf8(":/img/b24.png")));
      if(lcd3_value<limit[0])
        ui->bottomView2D->setPixmap(QPixmap(QString::fromUtf8(":/img/b25.png")));
      if(lcd3_value<limit[1])
        ui->bottomView2D->setPixmap(QPixmap(QString::fromUtf8(":/img/b26.png")));
      if(lcd3_value<limit[2])
        ui->bottomView2D->setPixmap(QPixmap(QString::fromUtf8(":/img/b27.png")));
      if(lcd3_value<limit[3])
        ui->bottomView2D->setPixmap(QPixmap(QString::fromUtf8(":/img/b28.png")));
      if(lcd3_value<limit[4])
        ui->bottomView2D->setPixmap(QPixmap(QString::fromUtf8(":/img/b29.png")));
    }
    if(lcd4_value<limit[4])
    {
      if(lcd3_value>=limit[0])
        ui->bottomView2D->setPixmap(QPixmap(QString::fromUtf8(":/img/b30.png")));
      if(lcd3_value<limit[0])
        ui->bottomView2D->setPixmap(QPixmap(QString::fromUtf8(":/img/b31.png")));
      if(lcd3_value<limit[1])
        ui->bottomView2D->setPixmap(QPixmap(QString::fromUtf8(":/img/b32.png")));
      if(lcd3_value<limit[2])
        ui->bottomView2D->setPixmap(QPixmap(QString::fromUtf8(":/img/b33.png")));
      if(lcd3_value<limit[3])
        ui->bottomView2D->setPixmap(QPixmap(QString::fromUtf8(":/img/b34.png")));
      if(lcd3_value<limit[4])
        ui->bottomView2D->setPixmap(QPixmap(QString::fromUtf8(":/img/b35.png")));
    }
}

MainWindow::~MainWindow() //destruktor
{
    delete ui;
}
