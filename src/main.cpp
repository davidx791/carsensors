/**
 * \file
 * \brief Moduł główny
 *
 * Plik zawiera definicje funkcji main
 */

#include "inc/mainwindow.h"
#include <QApplication>
#include <QWidget>

int main(int argc, char *argv[])
{
    QApplication App(argc, argv);
    MainWindow glowneOkno;
    glowneOkno.setWindowTitle("Czujniki zbliżeniowe");
    glowneOkno.setWindowIcon(QPixmap(QString::fromUtf8(":/img/auto.png")));
    glowneOkno.show();
    return App.exec();
}
